﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace WordleGame
{
    class Program
    {
        public static string wordsFilePath = @"..\..\..\Docs\words_es.txt";
        public static string language = "castellano";
        static void Main()
        {
           
            Console.WriteLine($"Bienvenido al juego Wordle en {language}.");
            Console.WriteLine("Elige una opción: ");
            Console.WriteLine("1. Jugar");
            Console.WriteLine("2. Cambiar idioma");
            Console.WriteLine("3. Muestra el Historial/Ránking");

            string input = Console.ReadLine();
            int option;

            while (!int.TryParse(input, out option) || option < 1 || option > 3)
            {
                Console.WriteLine("Por favor, introduce una opción válida.");
                input = Console.ReadLine();
            }

            switch (option)
            {
                case 1:
                    string wordToGuess = GetWordToGuess(wordsFilePath);
                    bool hasWon = false;

                    Console.WriteLine($"Debes adivinar una palabra de {wordToGuess.Length} letras en 6 intentos.");

                    for (int attempt = 1; attempt <= 6; attempt++)
                    {
                        Console.WriteLine($"\nIntento {attempt}: ");
                        string guess = Console.ReadLine().ToLower();

                        if (guess.Length != wordToGuess.Length)
                        {
                            Console.WriteLine($"Por favor, introduce una palabra de {wordToGuess.Length} letras.");
                            attempt--;
                            continue;
                        }

                        if (guess == wordToGuess)
                        {
                            Console.WriteLine("¡Ganaste!");
                            hasWon = true;
                            break;
                        }

                        for (int i = 0; i < wordToGuess.Length; i++)
                        {
                            if (wordToGuess[i] == guess[i])
                            {
                                Console.ForegroundColor = ConsoleColor.Green;
                                Console.Write(guess[i]);
                            }
                            else if (wordToGuess.Contains(guess[i]))
                            {
                                Console.ForegroundColor = ConsoleColor.Yellow;
                                Console.Write(guess[i]);
                            }
                            else
                            {
                                Console.ForegroundColor = ConsoleColor.Gray;
                                Console.Write(guess[i]);
                            }
                        }

                        Console.ForegroundColor = ConsoleColor.Gray;
                    }

                    if (!hasWon)
                    {
                        Console.WriteLine($"\nLo siento, has perdido. La palabra era {wordToGuess}.");
                    }

                    Console.ReadLine();
                    break;

                case 2:
                    Console.WriteLine("Selecciona un idioma:");
                    Console.WriteLine("1. Castellano");
                    Console.WriteLine("2. Inglés");

                    input = Console.ReadLine();
                    int languageOption;

                    while (!int.TryParse(input, out languageOption) || languageOption < 1 || languageOption > 2)
                    {
                        Console.WriteLine("Por favor, introduce una opción válida.");
                        input = Console.ReadLine();
                    }

                    switch (languageOption)
                    {
                        case 1:
                            wordsFilePath = @"..\..\..\Docs\words_es.txt";
                            language = "castellano";
                            break;
                        case 2:
                            wordsFilePath = @"..\..\..\Docs\words_en.txt";
                            language = "inglés";
                            break;
                    }

                    Console.WriteLine($"Idioma cambiado a {language}.");
                    Console.ReadLine();
                    Main();
                    break;

                case 3:
                    PrintRanking(GetRanking());
                    break;
            }


            static string GetWordToGuess(string filePath)
            {
                string[] lines = File.ReadAllLines(filePath);
                Random random = new Random();
                int randomIndex = random.Next(0, lines.Length);
                return lines[randomIndex].ToLower();
            }

            static string GetPlayerName()
            {
                Console.WriteLine("\n¡Felicidades por ganar el juego!");
                Console.WriteLine("Por favor, introduce tu nombre para guardar el resultado en el ranking:");
                return Console.ReadLine();
            }

            static void SaveResultToRanking(string playerName, int result)
            {
                string filePath = @"..\..\..\Docs\ranking.txt";
                string[] lines = { $"{playerName},{result}" };
                File.AppendAllLines(filePath, lines);
            }

            static List<(string, int)> GetRanking()
            {
                string filePath = @"..\..\..\Docs\ranking.txt";
                List<(string, int)> ranking = new List<(string, int)>();

                if (!File.Exists(filePath))
                {
                    File.Create(filePath).Dispose();
                    return ranking;
                }

                string[] lines = File.ReadAllLines(filePath);

                foreach (string line in lines)
                {
                    string[] values = line.Split(',');

                    if (values.Length != 2)
                    {
                        continue;
                    }

                    string playerName = values[0];
                    bool isValidResult = int.TryParse(values[1], out int result);

                    if (isValidResult)
                    {
                        ranking.Add((playerName, result));
                    }
                }

                return ranking.OrderByDescending(player => player.Item2).ToList();
            }
            static void PrintRanking(List<(string, int)> ranking)
            {
                if (ranking.Count == 0)
                {
                    Console.WriteLine("No hay resultados en el ranking");
                    return;
                }
                foreach (object line in ranking)
                {
                    Console.WriteLine(line);
                }
            }
        }
    }
}

